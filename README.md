# Longenesis Django API challenge

## How to run
This app is dockerized. To fully use it, please have `docker` and `docker-compose` installed first.

First run `docker-compose build` to build the application, then `docker-compose up` to start it. The api is now available on http://localhost/api/v1/surveys/

**Alternatively** you can set up virtualenv and run the API using Django built-in server.

```bash
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
python manage.py runserver
```
The API is now available on http://localhost:8000/api/v1/surveys/. Please note that using this approach, image retrieval won't work.

## Endpoints

Here is the list of available endpoints:

* /api/v1/rest-auth/registration/
* /api/v1/surveys/
* /api/v1/answers/
* /api/v1/answers-staff/
* /api/v1/users/

New users can self-register using http://localhost/api/v1/rest-auth/registration/.

Ordinary users can access survey questions using http://localhost/api/v1/surveys/.

Users can send answers using http://localhost/api/v1/answers/. Users see only their own previously sent answers.

Staff users can access answers using http://localhost/api/v1/answers-staff/. Existing answers can be modified using their IDs - http://localhost/api/v1/answers-staff/1/.
Staff users can also attach an image to a given answer.

Staff users can manage users using http://localhost/api/v1/users/. Note that superusers are not listed here.

Uploaded images are available publicly, however their filenames are listed only to staff users.


## Sample users

**There are two ordinary users**

username `user1`     \
email `user1@email.email`     \
password `MSTP9MpGDoux`

username `user2`     \
email `user2@email.email`     \
password `MSTP9MpGDoux`    \
key `751940fa3766f0638856e01c6342f8c1df547960`

**and one privileged/staff user**

username `staffuser1`     \
email `staffuser1@email.email`   \
password `MSTP9MpGDoux`   \
key `50b249581cd4006206c5acfb2934954e2c7f79fb`

**There is also a superuser:**

username `superhuman`   \
password `humansuper`   \
Use superuser to access django admin - http://localhost/admin/


## Tests
If you have this app running in docker:
```bash
# connect to the container
docker exec -it longenesis_app bash
# then run the tests
python manage.py test
```

Alternatively, if you have the app running in virtualenv:
```bash
python manage.py test
```
