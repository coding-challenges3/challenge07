from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from .models import Human, Survey, Answer
from .permissions import ReadOnlyForNonStaff, WriteOnlyForNonStaff
from .serializers import (
    SurveySerializer,
    AnswerSerializer,
    AnswerSerializerStaff,
    HumanSerializer)
from rest_framework.permissions import IsAdminUser


class SurveyViewSet(viewsets.ModelViewSet):
    queryset = Survey.objects.all().order_by('id')
    serializer_class = SurveySerializer
    permission_classes = (IsAuthenticated, ReadOnlyForNonStaff,)


class AnswerViewSet(viewsets.ModelViewSet):
    serializer_class = AnswerSerializer
    permission_classes = (IsAuthenticated, WriteOnlyForNonStaff,)

    def perform_create(self, serializer):
        serializer.save(human=self.request.user)

    def get_queryset(self, *args, **kwargs):
        return Answer.objects.filter(human=self.request.user)


class AnswerViewSetStaff(viewsets.ModelViewSet):
    queryset = Answer.objects.all().order_by('id')
    serializer_class = AnswerSerializerStaff
    permission_classes = (IsAuthenticated, IsAdminUser,)


class HumanViewSetStaff(viewsets.ModelViewSet):
    queryset = Human.objects.filter(is_superuser=False).order_by('id')
    serializer_class = HumanSerializer
    permission_classes = (IsAuthenticated, IsAdminUser,)

