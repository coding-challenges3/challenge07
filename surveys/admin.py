from django.contrib import admin
from .models import Human, Survey, Answer

admin.site.register(Human)
admin.site.register(Survey)
admin.site.register(Answer)
