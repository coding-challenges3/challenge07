from rest_framework import permissions

class ReadOnlyForNonStaff(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_staff:
            return True
        elif request.method in permissions.SAFE_METHODS:
            return True
        else:
            return False


class WriteOnlyForNonStaff(permissions.BasePermission):
    def has_permission(self, request, view):
        return True

    def has_object_permission(self, request, view, obj):
        if request.user.is_staff:
            return True
        return False
