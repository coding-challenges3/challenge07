from rest_framework.routers import SimpleRouter
from .views import (
    SurveyViewSet,
    AnswerViewSet,
    AnswerViewSetStaff,
    HumanViewSetStaff
)


router = SimpleRouter()
router.register('surveys', SurveyViewSet, basename='surveys')
router.register('answers', AnswerViewSet, basename='answers')
router.register('answers-staff', AnswerViewSetStaff, basename='answers_staff')
router.register('users', HumanViewSetStaff, basename='users')
urlpatterns = router.urls
