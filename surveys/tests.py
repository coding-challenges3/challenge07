from django.test import TestCase

# Create your tests here.

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from .models import Human, Survey, Answer

from rest_framework.test import force_authenticate
from rest_framework.test import APIRequestFactory

from .views import SurveyViewSet


class AccountTests(APITestCase):
    def test_create_account(self):
        """
        Ensure we can create a new account object.
        """
        url = reverse('rest_register')
        data = {
            "username": "martins",
            "email": "martins@email.email",
            "password1": "sZQf8ZutJDtt",
            "password2": "sZQf8ZutJDtt"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 201)


class SurveysUnauthorizedTests(APITestCase):
    def test_unauthorized_surwey(self):
        """
        Ensure that not-logged-in users get Error 403.
        """
        url = reverse('surveys-list')
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, 403)


class SurveysTests(APITestCase):
    def setUp(self):
        u = Human(username="testuser")
        u.save()

    def test_surwey(self):
        """
        Ensure we can access surveys after login
        """

        factory = APIRequestFactory()
        user = Human.objects.get(username='testuser')
        view = SurveyViewSet.as_view({'get': 'list'})

        # Make an authenticated request to the view...
        request = factory.get(reverse('surveys-list'))
        force_authenticate(request, user=user)
        response = view(request)
        self.assertEqual(response.status_code, 200)
