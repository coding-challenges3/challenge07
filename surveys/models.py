from django.contrib.auth.models import AbstractUser
from django.db import models
import uuid
from datetime import datetime, timezone
from django.conf import settings
import os


class Human(AbstractUser):
    nickname = models.CharField(max_length=250, null=True)
    favorite_color = models.CharField(max_length=250, null=True)
    favorite_food = models.CharField(max_length=250, null=True)


class AutoDateModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Survey(AutoDateModel):
    q1 = models.TextField("Question 1")
    q2 = models.TextField("Question 2")
    q3 = models.TextField("Question 3")


def uuid_image_name(instance, filename):
    import uuid

    upload_to = "images/"
    ext = filename.split(".")[-1]
    filename = "{}.{}".format(uuid.uuid4().hex, ext)
    return os.path.join(upload_to, filename)


class Answer(models.Model):
    survey = models.ForeignKey(
        Survey,
        on_delete=models.CASCADE,
        related_name='answer')
    human = models.ForeignKey(
        Human,
        on_delete=models.CASCADE,
        related_name='answer')
    a1 = models.TextField("Answer to Question 1", blank=True)
    a2 = models.TextField("Answer to Question 2", blank=True)
    a3 = models.TextField("Answer to Question 3", blank=True)
    image = models.FileField(
        upload_to=uuid_image_name,
        blank=True
    )
