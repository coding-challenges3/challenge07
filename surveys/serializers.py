from rest_framework import serializers
from .models import Human, Survey, Answer


class SurveySerializer(serializers.ModelSerializer):
    class Meta:
        model = Survey
        fields = ('id', 'q1', 'q2', 'q3')


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ('id', 'survey', 'a1', 'a2', 'a3')


class AnswerSerializerStaff(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ('id', 'survey', 'human', 'a1', 'a2', 'a3', 'image')


class HumanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Human
        fields = ('id', 'username', 'email', 'nickname', 'favorite_color', 'favorite_food', 'is_staff')
        read_only_fields = ('id', 'username', 'email',)
